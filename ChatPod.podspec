#
#  Be sure to run `pod spec lint ChatPod.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name             = 'ChatPod'
  s.version          = '1.0.0'
  s.summary          = 'A framework for chat functionality using Socket.IO in Swift.'
  s.homepage         = 'https://gitlab.com/mohamedaglan2225/chatpod-framework/-/tree/1.0.0?ref_type=heads'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Mohamed Aglan' => 'mohamedaglan.dev@email.com' }
  s.platform         = :ios, '11.0'
  s.source           = { :git => 'https://gitlab.com/mohamedaglan2225/chatpod-framework.git', :tag => s.version.to_s }
  s.source_files     = 'ChatPod.framework/Headers/*.h'
  s.vendored_frameworks = 'ChatPod.framework'
  s.frameworks       = 'UIKit'
  s.dependency       'Socket.IO-Client-Swift', '~> 15.0'
end