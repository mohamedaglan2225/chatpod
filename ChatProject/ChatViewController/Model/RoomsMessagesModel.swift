//
//  RoomsMessagesModel.swift
//  Shashty
//
//  Created by Mohamed Aglan on 5/19/22.
//

import Foundation

// MARK: - RoomsMessagesModel
struct RoomsMessagesModel: Codable {
    var messages: [RoomsMessage]?
}

// MARK: - RoomsMessage
struct RoomsMessage: Codable {
    var id: Int?
    var sender: Sender?
    var message, date: String?
    var image : String?
}

// MARK: - Sender
struct Sender: Codable {
    var id: Int?
    var name: String?
    var image: String?
    var phone, rate: String?
    var address: RoomsAddress?
    var bio, socketID: String?

    enum CodingKeys: String, CodingKey {
        case id, name, image, phone, rate, address, bio
        case socketID = "socket_id"
    }
}

// MARK: - RoomsAddress
struct RoomsAddress: Codable {
    var lat, lng, city, mapDesc: String?

    enum CodingKeys: String, CodingKey {
        case lat, lng, city
        case mapDesc = "map_desc"
    }
}
