//
//  UserRecieverCell.swift
//  Shashty
//
//  Created by Mohamed Aglan on 5/19/22.
//

import UIKit

class UserRecieverCell: UITableViewCell {
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageDate: UILabel!
    @IBOutlet weak var messageBody: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        messageView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMinYCorner]
        messageView.layer.cornerRadius = 16
    }
    
    //MARK: - Business Logic -
    func configureRoomsCell(model:RoomsMessage) {
        messageBody.text = model.message
        messageDate.text = model.date
//        messageImage.setWith(url: model.sender?.image)
    }
}
