//
//  UserSenderCell.swift
//  Shashty
//
//  Created by Mohamed Aglan on 5/19/22.
//

import UIKit

class UserSenderCell: UITableViewCell {
    
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var messageDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        messageView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMinYCorner]
        messageView.layer.cornerRadius = 16
    }
    
    func configCell(model:RoomsMessage) {
        messageLbl.text = model.message
        messageDate.text = model.date
    }
    
}
