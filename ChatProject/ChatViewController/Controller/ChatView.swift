//
//  ChatView.swift
//  ChatPod
//
//  Created by Mohamed Aglan on 6/22/23.
//

import UIKit
import SocketIO

class ChatView: UIViewController {
    
    //MARK: - IBOutLets -
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextField: UITextField!
    
        
    //MARK: - Properties -
    var roomsModel:[RoomsMessage] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    var userName = ""
    var userId = Int()
    var roomID:Int?
    var recieverId:Int?
    
    
    
    //MARK: - LifeCycle Events -
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        tableView.transform = CGAffineTransform(scaleX: 1, y: -1)
        navigationController?.navigationBar.tintColor = UIColor.black
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        ConnectToSocket()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        exitChat()
    }
    
    //MARK: - Business Logic -
    private func registerCell() {
        tableView.register(cellType: UserRecieverCell.self)
        tableView.register(cellType: UserSenderCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
    
    
    
    // MARK: - SOCKET CONNECTION -
    private func ConnectToSocket() {
        
        // check Socket connection
        if(SocketConnection.sharedInstance.socket.status == .notConnected){
            print("Not connected")
            SocketConnection.sharedInstance.manager.connect()
            SocketConnection.sharedInstance.socket.connect()
        }
        
        if(SocketConnection.sharedInstance.socket.status == .disconnected){
            print("Disconnected")
            SocketConnection.sharedInstance.manager.connect()
            SocketConnection.sharedInstance.socket.connect()
        }
        
        if(SocketConnection.sharedInstance.socket.status == .connecting){
            print("Trying To Connect...")
            SocketConnection.sharedInstance.manager.connect()
            SocketConnection.sharedInstance.socket.connect()
        }
        
        print(SocketConnection.sharedInstance.socket.status)
        
        
        SocketConnection.sharedInstance.socket.once(clientEvent: .connect) { (data, ack) in
            if self.roomID != 0 {
                let jsonDic = [
                    "room_id": "\(self.roomID!)",
                    "user_id": "\(self.userId)",
                    "user_type": "User"
                ]
                
                SocketConnection.sharedInstance.socket.emit("enterChat", jsonDic)
                self.SockeConFigration()
                print("💂🏻‍♀️AdddUssser")
                
            }
        }
        
        if(SocketConnection.sharedInstance.socket.status == .connected){
            if self.roomID != 0 {
                let jsonDic = [
                    "room_id": "\(self.roomID!)",
                    "user_id": "\(self.userId)",
                    "user_type": "User"
                ]
                
                SocketConnection.sharedInstance.socket.emit("enterChat", jsonDic)
                self.SockeConFigration()
                print("💂🏻‍♀️AdddUser")
                
            }
        }
        
        SocketConnection.sharedInstance.socket.on(clientEvent: .error) { (data, ack) in
            print("🍋Error")
            print("🍉\(data)")
        }
        
        SocketConnection.sharedInstance.socket.on(clientEvent: .disconnect) { (data, ack) in
            print("🍋disconnect")
            print("🍉\(data)")
            print("💂🏻‍♀️exitUser")
        }
        
        SocketConnection.sharedInstance.socket.on(clientEvent: .ping) { (data, ack) in
            print("🍋Ping")
            print("🍉\(data)")
        }
        
        SocketConnection.sharedInstance.socket.on(clientEvent: .reconnect) { (data, ack) in
            print("🍋reconnect")
            print("🍉\(data)")
        }
    }
    
    
    private func SockeConFigration() {
        SocketConnection.sharedInstance.socket.on("sendMessageRes") { (data, ack) in
            print("🌻\(data)")
            let dict = data[0] as! [String: Any]
            print("💙💙\(dict["message"] as? String ?? "")")
            let id = dict["id"] as? Int ?? 0
            let message = dict["body"] as? String ?? ""
            let name = dict["name"] as? String ?? ""
            let avatar = dict["avatar"] as? String ?? ""
            let createdDt = dict["created_dt"] as? String ?? ""
            let type = dict["type"] as? String ?? ""
            let _ = dict["room_id"] as? Int ?? 0
            let _ = dict["senderId"] as? Int ?? 0
            let _ = dict["receiver_id"] as? Int ?? 0
            let _ = dict["is_sender"] as? Bool ?? false
            let _ = dict["address"] as? Bool ?? false
            DispatchQueue.main.async {
                if type == "text" {
                    self.roomsModel.insert(RoomsMessage(id: id, sender: Sender(id: 0, name: name, image: avatar, phone: "", rate: "", address: nil, bio: "", socketID: ""), message: message, date: createdDt), at: 0)
                    self.tableView.reloadData()
                    self.tableView.scrollToTop()
                }
            }
        }
        
    }
    
    private func exitChat() {
        if self.roomID != 0 {
            let jsonDic = [
                "room_id": "\(self.roomID!)",
                "user_id": "\(self.userId)",
                "user_type": "User"
            ]
            SocketConnection.sharedInstance.socket.emit("exitChat", jsonDic)
            self.SockeConFigration()
            print("💂🏻‍♀️exitChat")
            SocketConnection.sharedInstance.socket.off(clientEvent: .connect)
            SocketConnection.sharedInstance.socket.off(clientEvent: .reconnect)
            SocketConnection.sharedInstance.socket.off("exitChat")
            SocketConnection.sharedInstance.socket.disconnect()
        }
        
    }
    
    
    
    @IBAction func sendMessageButton(_ sender: UIButton) {
        guard messageTextField.text != "" else {return}
        sendMessageApi(message:messageTextField.text!)
    }
    
    

}


extension ChatView {
    
    private func sendMessageApi(message:String) {
        let jsonDic = [
            "room_id": "\(roomID!)",
            "sender_id": "\(self.userId)",
            "sender_name": "\(self.userName)",
            "receiver_id": "\(recieverId!)",
            "sender_type":"User",
            "receiver_type":"Provider",
            "type": "text",
            "body": "\(message)",
        ]
        
        SocketConnection.sharedInstance.socket.emit("sendMessage", jsonDic)
        print("💂🏻‍♀️SentMessage \(jsonDic)")
        
        roomsModel.insert(RoomsMessage(id: recieverId, sender: Sender(id: self.userId, name: "", image: nil, phone: nil, rate: nil, address: nil, bio: nil, socketID: nil), message: message, date: ""), at: 0)
        
        //MARK: Edit
//        textHight.constant = 40
//        roomsModel.insert(RoomsMessage(id: recieverId, sender: nil, message: message, date: ""), at: 0)
//        messageTextView.text = ""
//        chatTableView.reloadData()
//        chatTableView.scrollToTop()
        
    }
    
    
}


//MARK: - Extensions -
extension ChatView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roomsModel.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if userId == roomsModel[indexPath.row].sender?.id {
            let cell = tableView.dequeueReusableCell(with: UserSenderCell.self, for: indexPath)
            cell.transform = CGAffineTransform(scaleX: 1, y: -1)
            cell.configCell(model: roomsModel[indexPath.row])
            return cell
        }else{
            let recieverCell = tableView.dequeueReusableCell(with: UserRecieverCell.self, for: indexPath)
            recieverCell.transform = CGAffineTransform(scaleX: 1, y: -1)
            recieverCell.configureRoomsCell(model: roomsModel[indexPath.row])
            return recieverCell
        }
    }
    
}


extension UIScrollView {
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
    }
}
