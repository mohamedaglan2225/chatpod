//
//  SocketConnection.swift
//  ChatPod
//
//  Created by Mohamed Aglan on 6/22/23.
//

import SocketIO

open class SocketConnection {
    public static let sharedInstance = SocketConnection()
    let manager : SocketManager
    public var socket : SocketIOClient
    
    private init() {
        manager = SocketManager(socketURL: URL(string: "YOUR-PROJECT-URL + PORT NUMBER")!, config: [.log(false)])
        socket = manager.defaultSocket
    }
}

